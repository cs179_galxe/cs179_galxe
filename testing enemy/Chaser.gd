extends KinematicBody2D

const UP = Vector2(0,-1)
var motion = Vector2(0,0)
var died = 0;
const GRAVITY = 600
onready var spawn = get_parent().get_parent().get_node("SpawnNode")
onready var player = get_parent().get_parent().get_node("Player")
var count = 0;

func _ready():
	set_process(true)
	pass

func _char_died():
	queue_free()
func _collision_manager(motion):
	randomize()
	if(motion.length() < 250):
		if(abs(player.position.x - self.position.x) + abs(player.position.y - self.position.y) < 150):
			motion.x = (player.position.x - self.position.x)
			if(motion.x < 0):
				motion.x = motion.x * 3
			else:
				motion.x = motion.x * 3
			motion.y = (player.position.y - self.position.y)
			if(motion.y < 0):
				motion.y = motion.y * 3
			else:
				motion.y = motion.y * 3
		"""else:
			motion = player.position - self.position
			if(motion.x < 0):
				motion.x = motion.x * rand_range(0,2)
			else:
				motion.x = motion.x * rand_range(0,2)
			if(motion.y < 0):
				motion.y = motion.y * rand_range(0,2)
			else:
				motion.y = motion.y * rand_range(0,2)"""
	else:
		motion.x = (spawn.position.x - self.position.x)*3
		motion.y = (spawn.position.y - self.position.y)*3
		if(abs(motion.x) + abs(motion.y) < 150):
			motion = Vector2(rand_range(0,3),rand_range(0,3));
#		else:
#			if(motion.x < 0):
#				motion.x = motion.x - rand_range(0,3)
#			else:
#				motion.x = motion.x + rand_range(0,3)
#			if(motion.y < 0):
#				motion.y = motion.y - rand_range(0,3)
#			else:
#				motion.y = motion.y + rand_range(0,3)
	self.move_and_slide(motion)
	pass
func _physics_process(delta):
	randomize()
	motion.x = (player.position.x - spawn.position.x)
	motion.y = (player.position.y - spawn.position.y)
	_collision_manager(motion)
	pass