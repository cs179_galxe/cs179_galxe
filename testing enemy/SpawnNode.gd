extends KinematicBody2D

const UP = Vector2(0, 0)
var motion = Vector2(0,0)
var died = 0;
const GRAVITY = 600
onready var target = get_parent().get_node("Player")
func _ready():
	set_process(true)
	pass

func _physics_process(delta):
	motion.x = (target.position.x - self.position.x) * 0.50
	motion.y = (target.position.y - self.position.y) * 0.50
	motion = move_and_slide(motion)
	pass