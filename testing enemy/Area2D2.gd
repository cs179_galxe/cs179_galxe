extends Area2D
var Health = 2
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Area2D2_area_entered(area):
	Health = Health - 1
	if(Health == 0):
		get_node("../../Enemy")._char_died()
	pass # replace with function body
