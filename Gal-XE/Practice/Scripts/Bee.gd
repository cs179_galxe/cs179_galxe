# Script to test player movement and animation changes

extends RigidBody2D

# member variables
var anim = ""
var AIR_ACCEL = 500
var AIR_MAX_VEL = 800

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	
func _integrate_forces(state):
	var lin_v = state.get_linear_velocity()
	var step = state.get_step()
	var new_anim = anim
	
	# get movement input
	var move_left = Input.is_action_pressed("player_left")
	var move_right = Input.is_action_pressed("player_right")
	var move_up = Input.is_action_pressed("player_up")
	var move_down = Input.is_action_pressed("player_down")
	
	# up-down movement
	if move_up and not move_down:
		if lin_v.y > -AIR_MAX_VEL:
			lin_v.y -= AIR_ACCEL * step
	elif move_down and not move_up:
		if lin_v.y < AIR_MAX_VEL:
			lin_v.y += AIR_ACCEL * step
	else:
		var yv = abs(lin_v.y)
		yv -= AIR_ACCEL * step
		if yv < 0:
			yv = 0
		lin_v.y = sign(lin_v.y) * yv
	
	# left-right movement
	if move_left and not move_right:
		new_anim = "bee_left"
		if lin_v.x > -AIR_MAX_VEL:
			lin_v.x -= AIR_ACCEL * step
	elif move_right and not move_left:
		new_anim = "bee_right"
		if lin_v.x < AIR_MAX_VEL:
			lin_v.x += AIR_ACCEL * step
	else:
		new_anim = "bee_center"
		var xv = abs(lin_v.x)
		xv -= AIR_ACCEL * step
		if xv < 0:
			xv = 0
		lin_v.x = sign(lin_v.x) * xv
	
	# Change animation
	if anim != new_anim:
		anim = new_anim
		$AnimationPlayer.play(anim)
	
	state.set_linear_velocity(lin_v)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
