# Script to notify when the player has reached the goal

extends RichTextLabel

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	#self.get_node("../../TileMap/HoneyZone").connect("body_entered", self, "_on_enter_honeyZone")
	pass

# runs when signal body_entered is sent from node HoneyZone
func _on_enter_honeyZone(body):
	self.clear()
	if body.get_name() == "Bee":
		self.add_text("You made it!")
	else:
		self.add_text("Fly to the yellow honey block")
	pass
