extends Node

onready var parent = get_tree().get_root().get_node("Level1")

onready var narrnode = get_node("../Narration_Node")

onready var progressplayer = get_node("../ProgressPlayer")

onready var wave1 = get_node("../Wave 1")
onready var wave2 = get_node("../Wave 2")
onready var wave3 = get_node("../Wave 3")
onready var wave4 = get_node("../Wave 4")
onready var wave5 = get_node("../Wave 5")
onready var crowd = get_node("../AnimationPlayer")

onready var player = get_node("../Player")
onready var gameover = get_node("../GameOver")
onready var gameoversound = get_node("../Gameoversound")
onready var audio = get_node("../AudioStreamPlayer")


onready var Player = get_node("../Player")

onready var endscreen = get_node("../tallyscreen")

onready var timenode = get_node("../CanvasLayer2/Time")
onready var timer = get_node("../Timer")

var rd1 = false
var rd2 = false
var rd3 = false
var rd4 = false
var rd5 = false

var ad1 = false
var ad2 = false
var ad3 = false
var ad4 = false
var ad5 = false

var curr_time = 0
var Bonus = 50000

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

# res://Sprites/Narration Assets/mechan_icon.png
# res://Sprites/Narration Assets/nova_icon.png
# res://Sprites/Narration Assets/richie_icon.png


func _ready():
	wave1.is_playing = true
	wave2.is_playing = false
	wave3.is_playing = false
	wave4.is_playing = false
	wave5.is_playing = false
	
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# check if children of waves are initialized
	if wave1.get_child_count() > 0 && rd1 == false:
		rd1 = true
		crowd.play("jump")
		narrnode.start_dialog("res://Dialogues/Level 2 Dialog/Dialog 1", "res://Sprites/Narration Assets/nova_icon.png")
	
	if wave2.get_child_count() > 0 && rd2 == false:
		rd2 = true
		
	if wave3.get_child_count() > 0 && rd3 == false:
		
		rd3 = true
		
	if wave4.get_child_count() > 0 && rd4 == false:
		rd4 = true
		
	#if wave5.get_child_count() > 0 && rd5 == false:
		#rd5 = true
	
	
	# if wave x finished move play animation
	if wave1.get_child_count() == 0 && wave1.is_playing == true && rd1 == true:
		progressplayer.play("Wave1")
		ad1 = true
		pass
	
	if wave2.get_child_count() == 0 && wave2.is_playing == true && rd2 == true:
		progressplayer.play("Wave2")
		ad2 = true
		pass
	
	if wave3.get_child_count() == 0 && wave3.is_playing == true && rd3 == true:
		progressplayer.play("Wave3")
		ad3 = true
		pass
	
	if wave4.get_child_count() == 0 && wave4.is_playing == true && rd4 == true:
		timer.stop()
		progressplayer.play("Wave4")
		ad4 = true
		pass
		
	#if wave5.get_child_count() == 0 && wave5.is_playing == true && rd5 == true:
		#progressplayer.play("Wave5")
		#ad5 = true
		#pass
		
		
	
	# if animation finished move to next wave, play appropriate dialogs and move positions if necessary
	if ad1 == true && progressplayer.is_playing() == false:
		ad1 = false
		wave2.is_playing = true
		wave1.is_playing = false
		narrnode.start_dialog("res://Dialogues/Level 2 Dialog/Dialog 2", "res://Sprites/Narration Assets/mechan_icon.png")
		pass
	
	if ad2 == true && progressplayer.is_playing() == false:
		ad2 = false
		#Player.position = Position2.get_position()
		progressplayer.play("Wave3Start")
		wave2.is_playing = false
		wave3.is_playing = true
		narrnode.start_dialog("res://Dialogues/Level 2 Dialog/Dialog 3", "res://Sprites/Narration Assets/richie_icon.png")
		pass
	
	if ad3 == true && progressplayer.is_playing() == false:
		ad3 = false
		wave3.is_playing = false
		wave4.is_playing = true
		narrnode.start_dialog("res://Dialogues/Level 2 Dialog/Dialog 4", "res://Sprites/Narration Assets/nova_icon.png")
		pass
	
	if ad4 == true && progressplayer.is_playing() == false:
		ad4 = false
		wave4.is_playing = false
		var total_points = (parent.cur_points * parent.multiplier) + Bonus
		endscreen.play_tally(total_points, 40000, 70000)
		pass
		
	#if ad5 == true && progressplayer.is_playing() == false:
		#ad5 = false
		#pass
		
	
	if rd1 == true && Player._health <= 0:
		audio.stop()
		gameover._start("res://Levels/Level2.tscn")
		
	if rd1 == true && Player._health <= 0:
		gameover._start("res://Levels/Level2.tscn")
	
	if parent.cur_points > 1000:
		get_node("../CanvasLayer2/Label2").visible = true;
	
	if parent.cur_points < 1000:
		get_node("../CanvasLayer2/Label2").visible = false;
	
	if Input.is_action_just_pressed("IncreaseHB") && parent.cur_points >= 1000:
		player._calc_health(-25)
		parent.cur_points -= 1000
		pass
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Timer_timeout():
	if rd1 == true:
		curr_time += 1
		timenode.set_text(str(curr_time))
		if Bonus > 0:
			Bonus -= 100
	pass # replace with function body
