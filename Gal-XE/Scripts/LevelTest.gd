extends Node

onready var Narr = get_node("../Narration_Node")
onready var ray1 = get_node("../TripleJump")
onready var ray2 = get_node("../PunchTutorial")
onready var ray3 = get_node("../GrabTutorial")
onready var Player = get_node("../Player")
onready var gameover = get_node("../GameOver")
onready var wave = get_node("../Wave")


var ready = false
var done = false

var ray1done = false
var ray2done = false
var ray3done = false

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

onready var w1_defeat = false

func _ready():
	#Wave1._make_enemies();
	pass

func _process(delta):
	if ray1.is_colliding() && !ray1done:
		ray1done = true
		Narr.start_dialog("res://Dialogues/Tutorial Dialog/Dialog2", "res://Sprites/Narration Assets/mechan_icon.png")
		
	if ray2.is_colliding() && !ray2done:
		ray2done = true
		Narr.start_dialog("res://Dialogues/Tutorial Dialog/Dialog3", "res://Sprites/Narration Assets/mechan_icon.png")
		
	if ray3.is_colliding() && !ray3done:
		Narr.start_dialog("res://Dialogues/Tutorial Dialog/Dialog4", "res://Sprites/Narration Assets/mechan_icon.png")
		ray3done = true
		
	if ready == false && wave.get_child_count() > 0:
		Narr.start_dialog("res://Dialogues/Tutorial Dialog/Dialog1", "res://Sprites/Narration Assets/mechan_icon.png")
		ready = true
	
	if wave.get_child_count() == 0 && done == false && ready == true: 
		Narr.start_dialog("res://Dialogues/Tutorial Dialog/Dialog5", "res://Sprites/Narration Assets/mechan_icon.png")
		done = true
	
	if done == true && Input.is_action_just_pressed("ui_accept"):
		get_tree().change_scene("res://Menus/TitleMenu.tscn")
		
	
	if ready == true && Player._health <= 0:
		gameover._start("res://Levels/Tutorial.tscn")
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	#if !w1_defeat && Wave1._get_enemies() <= 0:
	#	Wave3._make_enemies();
	#	w1_defeat = true;
	#if w1_defeat && Wave3._get_enemies() <= 0:
		#print("you did it!!");
	pass
