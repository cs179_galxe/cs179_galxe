extends RigidBody2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# variables to indicate state of enemy
var _life_state = T.Enemy.Life.ALIVE
var _hit_state = T.Enemy.Hit.NONE

# constant variables
export (float, 1) var _MAX_KBACK_TM = 0.1
export (float, 2) var _MAX_BOUNCE_TM = 0.5
export (int, 10) var _MAX_BOUNCE_CNT = 2
export (float, 100) var _GRAVITY = 20.0

# movement variables
var mot_test = Physics2DTestMotionResult.new()
var _base_mvmt = Vector2 (0, 0)
var _cur_mvmt = _base_mvmt setget mvmt_set, mvmt_get
var _bounce_cnt = 0
var _kback_tm = 1e20
var _bounce_tm = 1e20

# enemy stats
export (int, 100) var _max_health = 3
var _cur_health = 0 setget health_set, health_get
export (int, 1, 20) var _attack_pow = 1 setget ,attack_get
export (int, 1000) var _point_val = 100 setget ,point_get
export (int, 10) var _multiplier = 1 setget , mult_get

# setter for _mvmt_vect
func mvmt_set(new_mvmt, state = T.Enemy.Hit.NONE):
	if state != T.Enemy.Hit.BOUNCE:
		_cur_mvmt = new_mvmt
		# set the knockback timer to zero
		if state == T.Enemy.Hit.PUNCH:
			_kback_tm = 0
		_hit_state = state
	pass

# getter for _mvmt_vect
func mvmt_get():
	return _cur_mvmt
	pass

# setter for _health
# value to enter is the value to change the health by
# positive values decrease the health while negative values increase it
func health_set(decrease):
	_cur_health = _cur_health - decrease
	if _cur_health <= 0:
		_life_state = T.Enemy.Life.DEAD
	pass

# getter for _health
func health_get():
	return _cur_health
	pass

# getter for attack power
func attack_get():
	return _attack_pow
	pass

# getter for _point_val
func point_get():
	return _point_val
	pass

# gett for _multiplier
func mult_get():
	return _multiplier
	pass

func _ready():
	_life_state = T.Enemy.Life.ALIVE
	_cur_health = _max_health
	contacts_reported = 5
	#self.connect("body_entered", self, "_on_collision")
	pass

func _process(delta):
	if _life_state == T.Enemy.Life.ALIVE:
		# Decide what to do based on the hit state of the enemy
		match _hit_state:
			T.Enemy.Hit.PUNCH:
				if _kback_tm <= _MAX_KBACK_TM:
					_kback_tm += delta
				else:
					_kback_tm = 1e20
					_hit_state = T.Enemy.Hit.FALL
			T.Enemy.Hit.GRAPPLE:
				print ("")
			T.Enemy.Hit.BOUNCE:
				if _bounce_cnt < _MAX_BOUNCE_CNT:
					_bounce_cnt += 1
					_hit_state = T.Enemy.Hit.FALL
				else:
					_bounce_cnt = 0
					_hit_state = T.Enemy.Hit.NONE
			T.Enemy.Hit.FALL:
				_cur_mvmt.y += _GRAVITY * delta
				if _cur_mvmt.x > 0:
					_cur_mvmt.x = _cur_mvmt.x / 2
			_:
				_cur_mvmt = _base_mvmt
	
	else:
		# notify level/wave manager that it is dead
		self.queue_free()
	pass

func _integrate_forces(state):
	var cur_lin_vel = state.get_linear_velocity()
	var delta = state.get_step()
	if cur_lin_vel.length() < 0.01:
		_hit_state = T.Enemy.Hit.NONE
	cur_lin_vel = _cur_mvmt / delta
	state.set_linear_velocity(cur_lin_vel)
	
	# check to see if enemy will bump into wall or platform
	# if so, set the bounce
	if self.test_motion(cur_lin_vel, 0.08, mot_test):
		var col_obj = mot_test.collider
		if col_object.get_collision_layer_bit(T.Col_Layer.WALL) or col_object.get_collision_layer_bit(T.Col_Layer.PLATFORM):
			print ("")
			# 
	pass

# function to manage the handling of the body_entered signal
func _on_collision(col_object):
	# set the hit state to bounce if the enemy hits a wall or platform
	if col_object.get_collision_layer_bit(T.Col_Layer.WALL) or col_object.get_collision_layer_bit(T.Col_Layer.PLATFORM):
		_hit_state = T.Enemy.Hit.BOUNCE
	pass