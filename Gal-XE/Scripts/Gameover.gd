extends CanvasLayer

# class member variables go here, for example:
var index = 0
var check1 = false
var check2 = false
var pause = false
var play = true

var restartpath

onready var tree = get_tree()
onready var ap = get_node("AnimationPlayer")
onready var gg = get_node("game_over")
func _ready():
	check1 = true
	pass

func _process(delta):
	
	#check if game is paused
	if pause == true:
		# Moving up and down the menu
		if Input.is_action_just_pressed("ui_up"):
			if index > 0: 
				index -= 1
			else:
				index = 0
			
		if Input.is_action_just_pressed("ui_down"):
			if index < 1:
				index += 1
			else: 
				index = 1
		# Condition statements for animation
		if index == 0 && check1:
			ap.play("Menu1")
			check1 = false
			check2 = true
		if index == 1 && check2:
			ap.play("Menu2")
			check1 = true
			check2 = false
			
		if Input.is_action_just_pressed("ui_accept"):
			var tree = get_tree()
			tree.paused = false
			if index == 0:
				tree.change_scene(restartpath)
			elif index == 1:
				get_tree().change_scene("res://Menus/TitleMenu.tscn")
	
	pass

func _start(path):
	get_node("Popup").show()
	gg.play()
	tree.paused = true
	pause = true
	restartpath = path
	pass
