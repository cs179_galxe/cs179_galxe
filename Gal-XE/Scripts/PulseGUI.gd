extends HBoxContainer

# reference to player
onready var _player_ref = self.get_node("../")

# reference to gui textures
var _empty_texture = preload("res://Sprites/GUI_assets/pulse_gui_empty.png")
var _full_texture = preload("res://Sprites/GUI_assets/pulse_gui_full.png")
var _red_texture = preload("res://Sprites/GUI_assets/pulse_gui_red.png")

# dimensions of gui elements
export (Vector2) var _gui_elem_size = Vector2(50, 50)

# array of children
var _pulse_dots

# player pulse count
var _pulse_cnt = 0

func _ready():
	# Get the pulse count from player
	_pulse_cnt = _player_ref._MAX_PULSE_CNT
	
	# make the container big enough to hold all gui elements
	self.rect_min_size = Vector2(_gui_elem_size.x * _pulse_cnt, _gui_elem_size.y)
	
	# position the contaioner so it is center with the player
	self.rect_position = Vector2(-(_gui_elem_size.x * _pulse_cnt)/2, 71)
	
	var t_rect
	# create new instances of textures
	for i in range(_pulse_cnt):
		t_rect = TextureRect.new()
		t_rect.texture = _full_texture
		t_rect.expand = true
		t_rect.stretch_mode = TextureRect.STRETCH_SCALE
		t_rect.rect_min_size = _gui_elem_size
		var rect = t_rect.get_rect()
		self.add_child(t_rect)
	
	_pulse_dots = self.get_children()
	pass

# signal function to handle Player's pulse change signal
func _on_pulse_change(pulses_used):
	# update the gui
	# print all red dots when no pulses are left
	if pulses_used >= _pulse_cnt:
		for dot in _pulse_dots:
			dot.texture = _red_texture
	else:
		for i in range(_pulse_cnt):
			# print empty dots for used pulses
			if i < (_pulse_cnt - pulses_used):
				_pulse_dots[i].texture = _full_texture
			# print full dots for available pulses
			else:
				_pulse_dots[i].texture = _empty_texture
	pass
