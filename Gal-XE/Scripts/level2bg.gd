extends CanvasLayer

onready var bg = get_node("bg")
onready var lights = get_node("lights")
onready var screen = get_node("screen")

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	bg.play("bg")
	lights.play("lights")
	screen.play("main")
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
