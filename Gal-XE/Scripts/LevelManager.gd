extends Node
# container for various enumerators
const T = preload("res://Scripts/Types.gd")

signal update_point_mult (cur_pts, total_pts, mult)

# variables for point statistics
var total_points = 0
var cur_points = 0
var multiplier = 1

# variables to keep track of updates to point and multiplier values
var prev_points = cur_points
var prev_mult = multiplier

# container and variables for wave nodes
var waves = []
var cur_wave_idx = 0 setget ,cur_wave_get
var wave_cnt = 0

# gets the index of the current wave
# starts from 1 and goes to the highest wave number
func cur_wave_get():
	return cur_wave_idx + 1
	pass

# variables to keep track of enemies
func _ready():
	var children = self.get_children()
	for node in children:
		if node.get_name().begins_with("Wave"):
			waves.append(node)
	wave_cnt = waves.size()
	waves[cur_wave_idx].activate_wave(true)
	self.print_pts()
	pass

# check to see if the point and/or multiplier values have changed since the last frame
func _process(delta):
	if prev_points != cur_points or prev_mult != multiplier:
		# conduct calculations on what the values should be
		self.emit_signal("update_point_mult", cur_points, total_points, multiplier)
	
	# update the previous values to the current values
	prev_points = cur_points
	prev_mult = multiplier
	
	# update which wave is to be activated
	if waves[cur_wave_idx].has_method("is_activated"):
		# change current wave
		if not waves[cur_wave_idx].is_activated():
			waves[cur_wave_idx].activate_wave(false)
			if wave_cnt > (cur_wave_idx + 1):
				cur_wave_idx += 1
				waves[cur_wave_idx].activate_wave(true)
			else:
				print ("")
	pass

func print_pts():
	print ("cur pts: ", cur_points)
	print ("mult: ", multiplier)
	print ("total: ", total_points)
	print ("\n")
	pass

# function to update point and multiplier values when the current wave updates its values
func update_status(points, mult):
	cur_points += points
	multiplier += mult
	self.print_pts()
	pass

# function to reset the current multiplier value when the player signals it is hit
func on_multiplier_reset():
	total_points += cur_points * multiplier
	cur_points = 0
	multiplier = 1
	pass
func dec_multiplier():
	if multiplier > 1:
		multiplier = multiplier - 1
	else:
		multiplier = 1
	pass
