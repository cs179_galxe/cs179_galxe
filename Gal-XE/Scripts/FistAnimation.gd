extends Node2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")
onready var _player_anim = get_parent().get_parent().get_node("Player").get_node("invul")
onready var grab = get_node("grab")
# denotes condition of state
var _cur_punch_state = T.Player.Action.NO_PUNCH setget ,punch_state_get
var _cur_input_state = T.Player.Input.READY

# stores if only left fist was pressed or only right fist was pressed
var _only_lft_punch = false

# grab timer variables
var _GRAB_TM = 0.02
var _cur_grab_tm = 1e20

# animation variables
var _anim = ""
var _anim_player = null

# getter for _cur_punch_state
func punch_state_get():
	return _cur_punch_state

func _ready():
	# get the animation player
	_anim_player = self.get_node("FistAnimations")
	pass

func _process(delta):
	var new_anim
	self._input_helper(delta)
	
	# set the appropriate animation to play when the grab animation is not being played
	if not _anim_player.is_playing() or _cur_punch_state != T.Player.Action.GRAB:
		if _cur_punch_state == T.Player.Action.LFT_PUNCH:
			new_anim = "punchFist1"
		elif _cur_punch_state == T.Player.Action.RGT_PUNCH:
			new_anim = "punchFist2"
		elif _cur_punch_state == T.Player.Action.GRAB:
			new_anim = "grab"
		else:
			new_anim = ""
		self._anim_helper(new_anim)
	pass

# helper function to handle input detection
func _input_helper(delta):
	# variables to recieve player input
	var lft_in
	var rgt_in
	var lft_hold
	var rgt_hold
	var lft_rel
	var rgt_rel
	
	# ignore input if player is performing grab action
	if _cur_punch_state == T.Player.Action.GRAB and _anim_player.is_playing():
		return
	# reset the punch state when the grab aniamation stops playing
	elif _cur_punch_state == T.Player.Action.GRAB and not _anim_player.is_playing():
		grab.play()
		_cur_punch_state = T.Player.Action.NO_PUNCH
		return
	
	# ready to recieve fist input
	if _cur_input_state == T.Player.Input.READY:
		lft_in = Input.is_action_just_pressed("punch_lft")
		rgt_in = Input.is_action_just_pressed("punch_rgt")
		
		# prepare to wait for additional input when one fist input is given
		if lft_in and not rgt_in:
			_cur_input_state = T.Player.WAITING
			_cur_grab_tm = 0
			_only_lft_punch = true
		elif rgt_in and not lft_in:
			_cur_input_state = T.Player.Input.WAITING
			_cur_grab_tm = 0
			_only_lft_punch = false
		
		# prepare grab animation for animation player when input recieved
		elif lft_in and rgt_in:
			_cur_punch_state = T.Player.Action.GRAB
		
		# do not play animations when no input is entered
		else:
			_cur_punch_state = T.Player.Action.NO_PUNCH
	# waiting for grab input
	else:
		# check for grab input
		if _cur_grab_tm < _GRAB_TM:
			# runs if only the left trigger was pressed in ready state
			if _only_lft_punch:
				lft_rel = Input.is_action_just_released("punch_lft")
				lft_hold = Input.is_action_pressed("punch_lft")
				rgt_in = Input.is_action_just_pressed("punch_rgt")
				
				# prepare left punch animation for animation player if left input
				# is released before recieving right input
				if lft_rel:
					_cur_punch_state = T.Player.Action.LFT_PUNCH
					_cur_input_state = T.Player.Input.READY
				
				# prepare grab animation for animation player if right input
				# is pressed while left input is being held
				elif lft_hold:
					if rgt_in:
						_cur_punch_state = T.Player.Action.GRAB
						_cur_input_state = T.Player.Input.READY
			
			# runs if only the right trigger was pressed in ready state
			else:
				rgt_rel = Input.is_action_just_released("punch_rgt")
				rgt_hold = Input.is_action_pressed("punch_rgt")
				lft_in = Input.is_action_just_pressed("punch_lft")
				
				# prepare right punch animation for animation player if right input
				# is released before recieving left input
				if rgt_rel:
					_cur_punch_state = T.Player.Action.RGT_PUNCH
					_cur_input_state = T.Player.Input.READY
					return
				
				# prepare grab animation for animation player if left input
				# is pressed while right input is being held
				elif rgt_hold:
					if lft_in:
						_cur_punch_state = T.Player.Action.GRAB
						_cur_input_state = T.Player.Input.READY
				
			_cur_grab_tm += delta
		
		# prepare single punch animation for animation player
		# if input for grab was not recieved
		else:
			if _only_lft_punch:
				_cur_punch_state = T.Player.Action.LFT_PUNCH
			else:
				_cur_punch_state = T.Player.Action.RGT_PUNCH
			_cur_grab_tm = 1e20
			_cur_input_state = T.Player.Input.READY
	pass

# helper function to play animations
func _anim_helper(new_anim):
	if _anim != new_anim:
		_anim = new_anim
		if _anim != "":
			_anim_player.play(_anim)
	pass