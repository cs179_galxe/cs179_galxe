extends CanvasLayer

# class member variables go here, for example:
var index = 0
var check1 = false
var check2 = false
var pause = false

onready var tree = get_tree()
onready var ap = get_node("AnimationPlayer")

func _ready():
	check1 = true
	pass

func _process(delta):
	if Input.is_action_just_pressed("ui_menu"):
		get_node("Popup").show()
		tree.paused = true
		pause = true
		index = 0
	
	if pause == true:
		# Moving up and down the menu
		if Input.is_action_just_pressed("ui_up"):
			if index > 0:
				index -= 1
			else:
				index = 0
			
		if Input.is_action_just_pressed("ui_down"):
			if index < 1:
				index += 1
			else: 
				index = 1
	
		# Condition statements for animation
		if index == 0 && check1:
			ap.play("resume")
			check1 = false
			check2 = true
		if index == 1 && check2:
			ap.play("quit")
			check1 = true
			check2 = false
		
		if Input.is_action_just_pressed("ui_accept"):
			if index == 0:
				get_node("Popup").hide()
				var tree = get_tree()
				tree.paused = false
			if index == 1:
				var tree = get_tree()
				tree.paused = false
				get_tree().change_scene("res://Menus/TitleMenu.tscn")
	
	pass
