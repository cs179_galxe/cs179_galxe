extends CanvasLayer

onready var left_bubble = get_node("LeftBubbleDialog")
onready var right_bubble = get_node("RightBubbleDialog")
onready var lbubblesplayer = get_node("LeftBubblesPlayer")
onready var rbubblesplayer = get_node("RightBubblesPlayer")
onready var nextplayer = get_node("nextplayer")
onready var riconplayer = get_node("righticonplayer")
onready var ricon = get_node("righticon")
onready var Timer2 = get_node("Timer2")

var dialog_on = false
var order = []
var line_counter = 0
var current_line = 0
var prev = 0
var timerup = false
var timerwait = false


# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	lbubblesplayer.play("none")
	#start_dialog("res://Dialogues/Test Dialogs", "res://Sprites/Narration Assets/nova_icon.png")
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	

func start_dialog(dialogpath, iconpath):
	Timer2.stop()
	timerwait = false
	order.clear()
	nextplayer.play("not_next")
	lbubblesplayer.play("none")
	line_counter = 0
	current_line = 0
	left_bubble._empty()
	right_bubble._empty()
	riconplayer.play("right icon exit")
	prev = 0
	
	dialog_on = true
	
	left_bubble._start_dialog(dialogpath + "/Left_Bubble.gd")
	right_bubble._start_dialog(dialogpath + "/Right_Bubble.gd")
	
	var mytexture = load(iconpath)
	ricon.set_texture(mytexture)
	
	riconplayer.play("right icon entrance")
	
	
	
	var file = File.new()
	file.open(dialogpath + "/Order.gd", file.READ)
	
	# Read file into array
	line_counter = 0
	while !file.eof_reached():
		order.append(file.get_line())
		line_counter += 1
	
	file.close()
	
	if order[current_line] == "right":
		rbubblesplayer.play("right_open")
		right_bubble._play_dialog()
		prev = 1
	elif order[current_line] == "left":
		lbubblesplayer.play("left_open")
		left_bubble._play_dialog()
		prev = 2


func _process(delta):
	if Input.is_action_just_pressed("ui_accept") || timerup == true:
		timerup = false
		timerwait = false
		Timer2.stop()
		if dialog_on == true:
			current_line += 1
			if current_line < line_counter:
				if order[current_line] == "right":
					if prev == 2 || prev == 0:
						left_bubble._close_bubble()
						lbubblesplayer.play("left_close")
					rbubblesplayer.play("right_open")
					right_bubble._play_dialog()
					prev = 1
				elif order[current_line] == "left":
					if prev == 1 || prev == 0:
						right_bubble._close_bubble()
						rbubblesplayer.play("right_close")
					lbubblesplayer.play("left_open")
					left_bubble._play_dialog()
					prev = 2
			else:
				order.clear()
				nextplayer.play("not_next")
				line_counter = 0
				current_line = 0
				left_bubble._empty()
				right_bubble._empty()
				riconplayer.play("right icon exit")
				if prev == 2:
					lbubblesplayer.play("left_close")
				elif prev == 1:
					rbubblesplayer.play("right_close")
				lbubblesplayer.play("none")
				prev = 0
				dialog_on = false
		pass
	pass
	if dialog_on == true  && timerwait == false:
		if prev == 1 && right_bubble._line_done() == true:
			nextplayer.play("right_next")
			Timer2.start()
			timerwait = true
		elif prev == 2 && left_bubble._line_done() == true:
			nextplayer.play("left_next")
			Timer2.start()
			timerwait = true
		else:
			nextplayer.play("not_next")
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Timer2_timeout():
	timerup = true
	pass # replace with function body
