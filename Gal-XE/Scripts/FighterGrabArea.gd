extends Area2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# time variables
const _GRAB_TM = 0.2
var _cur_grab_tm = 1e20

# container for grabbed player
var grabbed_player = []

# reference to FighterFists object
onready var _fists_node = self.get_node("../../../../../")

# reference to FistCenter object
onready var _fist_center = self.get_node("../FistCenter")

# reference to previous state of fists in last frame
var _prev_punch_state

# state of fist ani

func _ready():
	self.connect("body_entered", self, "_player_grab")
	self.connect("body_exited", self, "_player_escape")
	if _fists_node.has_method("punch_state_get"):
		_prev_punch_state = _fists_node.punch_state_get()
	pass

func _process(delta):
	var cur_punch_state = T.Player.Action.NO_PUNCH
	if _fists_node.has_method("punch_state_get"):
		cur_punch_state = _fists_node.punch_state_get()
	
	# activate collisions when the grab state is active
	if cur_punch_state == T.Player.Action.GRAB:
		# check previous state to see if a new grab action has started
		# if so, activate collisions
		if _prev_punch_state != cur_punch_state:
			_cur_grab_tm = 0
			self.set_collision_mask_bit(T.Col_Layer.PLAYER, true)
		# activate collisions while the grabbing portion of the grab animation
		# is being played
		elif _cur_grab_tm < _GRAB_TM:
			_cur_grab_tm += delta
			self.set_collision_mask_bit(T.Col_Layer.PLAYER, true)
		# deactivate collisions when the grabbing part of the grab animation
		# has finished playing
		else:
			_cur_grab_tm = 1e20
			self._end_grab()
			self.set_collision_mask_bit(T.Col_Layer.PLAYER, false)
	
	# deactivate collisions when the grab state is inert
	else:
		self.set_collision_mask_bit(T.Col_Layer.PLAYER, false)
	
	_prev_punch_state = cur_punch_state
	self._move_player()
	pass

# sets grabbed enemy movement upon completeion of grabbing protion
# of grab animation
func _end_grab():
	var new_mvmt
	if _fists_node.get_parent().has_method("mvmt_get"):
		new_mvmt = _fists_node.get_parent().mvmt_get()
		if grabbed_player[0].has_method("mvmt_set"):
			grabbed_player[0].mvmt_set(new_mvmt, T.Enemy.Hit.FALL)
	grabbed_player.clear()
	pass

# updates movement for grabbed player
func _move_player():
	# get global position of the center of the fist
	var grab_origin = _fist_center.get_global_transform().get_origin()
	var player_origin
	var grab_mvmt
	
	# move grabbed player
	# get the global position of the enemy
	if grabbed_player:
		player_origin = grabbed_player[0].get_global_transform().get_origin()
		
		# get the direction of the vector from the enemy to the fist center
		grab_mvmt = (grab_origin - player_origin).normalized() * 17
		
		# add fighter movement to enemy grab movement
		if _fists_node.get_parent().has_method("mvmt_get"):
			grab_mvmt += self._fists_node.get_parent().mvmt_get()
		
		if grabbed_player[0].has_method("mvmt_set"):
			grabbed_player[0].mvmt_set(grab_mvmt, T.Enemy.Hit.GRAPPLE)
	pass

# signal function to detect enemies hit by the grab
func _player_grab(player):
	grabbed_player.append(player)
	# make fighter invisible to player
	self.set_collision_mask_bit(T.Col_Layer.PLAYER, false)
	self.set_collision_layer_bit(T.Col_Layer.ENEMY, false)
	self._move_player()
	pass

# signal function to detect enemies that escape grab
func _player_escape(player):
	# make fighter visible to player
	self.set_collision_mask_bit(T.Col_Layer.PLAYER, true)
	self.set_collision_layer_bit(T.Col_Layer.ENEMY, true)
	# remove the player from the grab list
	grabbed_player.erase(player)
	pass