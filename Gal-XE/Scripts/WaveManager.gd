extends Node

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# point values for wave
#var cur_points = 0
#var cur_mult = 0

# whether or not this wave is active
var activate = false
var is_playing = false setget activate_wave, is_activated

# stores all enemies in current wave
var enemies = []

# setter function for activate
func activate_wave(yes):
	is_playing = yes
	pass

# getter function for activate
func is_activated():
	return is_playing
	pass

func _ready():
	enemies = self.get_children()
	self._deactivate_enemy()
	pass

func _process(delta):
	if is_playing:
		# activate enemies the first time is_playing is set to true
		if not activate:
			self._activate_enemy()
		
		# check to see if there are still enemies left in wave
		enemies = self.get_children()
		activate = not enemies.empty()
		is_playing = activate
	pass

# Helper functions below:
# activates/show all the enemies
func _activate_enemy():
	for enemy in enemies:
		self.add_child(enemy)
	pass

# deactivates/hides all the enemies
func _deactivate_enemy():
	for enemy in enemies:
		self.remove_child(enemy)
	pass

# function to update point values when an enemy dies
# called by enemy that is killed
func update_status(enemy):
	var cur_points = 0
	var cur_mult = 1
	if enemy.has_method("point_get"):
		cur_points = enemy.point_get()
	if enemy.has_method("mult_get"):
		cur_mult = enemy.mult_get()
	# Inform the level manager of the change in score
	if self.get_parent().has_method("update_status"):
		self.get_parent().update_status(cur_points, cur_mult)
	pass