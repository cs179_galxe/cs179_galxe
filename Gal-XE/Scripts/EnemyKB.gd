extends KinematicBody2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# variables to indicate state of enemy
var _life_state = T.Enemy.Life.ALIVE
var _hit_state = T.Enemy.Hit.FALL setget ,hit_get

# constant variables
export (float, 1) var _MAX_KBACK_TM = 0.1
export (float, 2) var _MAX_BOUNCE_TM = 0.5
export (int, 100) var _MAX_BOUNCE_CNT = 7
export (float, 100) var _GRAVITY = 20.0

# movement variables
var _base_mvmt = Vector2 (0, 0)
var _cur_mvmt setget mvmt_set, mvmt_get
var _bounce_cnt = 0
var _kback_tm = 1e20
var _bounce_tm = 1e20

# enemy stats
export (int, 100) var _max_health = 3
var _cur_health = 0 setget health_set, health_get
export (int, 0, 20) var _attack_pow = 1 setget ,attack_get
export (int, 1000) var _point_val = 100 setget ,point_get
export (int, 10) var _multiplier = 1 setget , mult_get

# getter for _hit
# returns the current hit state the enemy is in
func hit_get():
	return _hit_state
	pass

# setter for _mvmt_vect
# sets the movement vector and the hit state
func mvmt_set(new_mvmt = _base_mvmt, state = T.Enemy.Hit.NONE):
	if state != T.Enemy.Hit.BOUNCE:
		_cur_mvmt = new_mvmt
		# set the knockback timer to zero
		if state == T.Enemy.Hit.PUNCH:
			_kback_tm = 0
		_hit_state = state
	pass

# getter for _cur_mvmt
# returns the current movement vector of the enemy
func mvmt_get():
	return _cur_mvmt
	pass

# setter for _health
# value to enter is the value to change the health by
# positive values decrease the health while negative values increase it
func health_set(decrease):
	_cur_health = _cur_health - decrease
	if _cur_health <= 0:
		_life_state = T.Enemy.Life.DEAD
	pass

# getter for current health of enemy
func health_get():
	return _cur_health
	pass

# getter for attack power
func attack_get():
	return _attack_pow
	pass

# getter for point value of enemy
func point_get():
	return _point_val
	pass

# gett for multiplier value of enemy
func mult_get():
	return _multiplier
	pass

func _ready():
	_life_state = T.Enemy.Life.ALIVE
	_cur_health = _max_health
	_cur_mvmt = _base_mvmt
	pass

func _physics_process(delta):
	self._base_physics_process(delta)
	pass

# helper function to manage collisions
func _collision_manager():
	_base_collision_manager()
	pass

# base version of _physics_process function
func _base_physics_process(delta):
	if _life_state == T.Enemy.Life.ALIVE:
		# Decide what to do based on the hit state of the enemy
		match _hit_state:
			T.Enemy.Hit.PUNCH:
				if _kback_tm <= _MAX_KBACK_TM:
					_kback_tm += delta
				else:
					_kback_tm = 1e20
					_hit_state = T.Enemy.Hit.FALL
			T.Enemy.Hit.GRAPPLE:
				_hit_state = T.Enemy.Hit.GRAPPLE
			T.Enemy.Hit.BOUNCE:
				if _bounce_cnt < _MAX_BOUNCE_CNT:
					_bounce_cnt += 1
					_hit_state = T.Enemy.Hit.FALL
				else:
					_bounce_cnt = 0
					_hit_state = T.Enemy.Hit.NONE
			T.Enemy.Hit.FALL:
				if _cur_mvmt.y < -0.15:
					_cur_mvmt.y = _cur_mvmt.y / 2
				else:
					_cur_mvmt.y += _GRAVITY * delta
				#if _cur_mvmt.x > 0.05:
				#	_cur_mvmt.x = _cur_mvmt.x / 1.5
			_:
				self._default_move()
		self._collision_manager()
	
	else:
		# notify level/wave manager that it is dead
		if self.get_parent().has_method("update_status") and not self.is_queued_for_deletion():
			self.get_parent().update_status(self)
		self.queue_free()
	pass

# base version of function to manage default movement
func _default_move():
	_cur_mvmt = _base_mvmt
	pass

# base version of helper function for managing collisions
func _base_collision_manager():
	var collision = self.move_and_collide(_cur_mvmt)
	if collision:
		var col_obj = collision.collider
		
		# deal damage to player if enemy bumps into player
		# enemy must be moving into player for godot to register that
		# enemy bumped into player
		if col_obj.get_collision_layer_bit(T.Col_Layer.PLAYER):
			# update player health if object has the function to do so
			if col_obj.has_method("health_set"):
				# deal damage if enemy is not being grabbed by player
				if _hit_state != T.Enemy.Hit.GRAPPLE:
					col_obj.health_set(_attack_pow)
		
		# bounce off of the player, platforms, and walls
		if not col_obj.get_collision_layer_bit(T.Col_Layer.ENEMY):
			_hit_state = T.Enemy.Hit.BOUNCE
			
			# reset the various timers
			_kback_tm = 1e20
			
			_cur_mvmt = _cur_mvmt.bounce(collision.normal) / 3
			var reflect = collision.remainder.bounce(collision.normal)
			self.move_and_collide(reflect)
	pass