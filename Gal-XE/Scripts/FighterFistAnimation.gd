extends Node2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# signal to indicate when a player has been punched
signal player_hit

# denotes condition of state
var _cur_punch_state = T.Player.Action.NO_PUNCH setget ,punch_state_get
var _prev_punch_state = T.Player.Action.NO_PUNCH
var _cur_signal_state = T.Player.Action.NO_PUNCH

# animation variables
var _anim = ""
var _anim_player = null

# getter for _cur_punch_state
func punch_state_get():
	return _cur_punch_state

func _ready():
	# get the animation player
	_anim_player = self.get_node("FistAnimations")
	pass

func _process(delta):
	# state machine for fighter attack animations
	match _cur_punch_state:
		T.Player.Action.LFT_PUNCH:
			if _cur_signal_state != T.Player.Action.PAUSE_PUNCH:
				_cur_punch_state = _cur_signal_state
			elif _prev_punch_state == T.Player.Action.PAUSE_PUNCH:
				self.emit_signal("player_hit")
			# go back to pause state when punch animation has finished
			elif not _anim_player.is_playing():
				_cur_punch_state = T.Player.Action.PAUSE_PUNCH
			_prev_punch_state = T.Player.Action.LFT_PUNCH
		T.Player.Action.RGT_PUNCH:
			if _cur_signal_state != T.Player.Action.PAUSE_PUNCH:
				_cur_punch_state = _cur_signal_state
			elif _prev_punch_state == T.Player.Action.PAUSE_PUNCH:
				self.emit_signal("player_hit")
			# go back to pause state when punch animation has finished
			elif not _anim_player.is_playing():
				_cur_punch_state = T.Player.Action.PAUSE_PUNCH
			_prev_punch_state = T.Player.Action.RGT_PUNCH
		T.Player.Action.PAUSE_PUNCH:
			if _cur_punch_state != _cur_signal_state:
				_cur_punch_state = _cur_signal_state
			# punch with right when previously punched with left
			elif _prev_punch_state == T.Player.Action.LFT_PUNCH:
				_cur_punch_state = T.Player.Action.RGT_PUNCH
			# punch with left when previously punched with right
			elif _prev_punch_state == T.Player.Action.RGT_PUNCH:
				_cur_punch_state = T.Player.Action.LFT_PUNCH
			# randomly choose which fist to punch with first when first starting punch chain
			else:
				randomize()
				var strt_fist = randi() % 2 + 1
				if strt_fist == 1:
					_cur_punch_state = T.Player.Action.LFT_PUNCH
				else:
					_cur_punch_state = T.Player.Action.RGT_PUNCH
			_prev_punch_state = T.Player.Action.PAUSE_PUNCH
		_:
			_prev_punch_state = _cur_punch_state
			_cur_punch_state = _cur_signal_state
	
	self._play_anim()
	pass

# helper function to set and play animations
func _play_anim():
	# set the appropriate animation to play when an animation is not being played
	var new_anim
	if not _anim_player.is_playing():
		if _cur_punch_state == T.Player.Action.LFT_PUNCH:
			new_anim = "punchFist1"
		elif _cur_punch_state == T.Player.Action.RGT_PUNCH:
			new_anim = "punchFist2"
		elif _cur_punch_state == T.Player.Action.GRAB:
			new_anim = "grab"
		else:
			new_anim = ""
		self._anim_helper(new_anim)
	pass

# helper function to play animations
func _anim_helper(new_anim):
	if _anim != new_anim:
		_anim = new_anim
		if _anim != "":
			_anim_player.play(_anim)
	pass

# signal function to detect when punches are proc'd
func punch_proc():
	_cur_signal_state = T.Player.Action.PAUSE_PUNCH
	pass

# signal function to detect when a grab is proc'd
func grab_proc():
	_cur_signal_state = T.Player.Action.GRAB
	pass

# signal function to detect when to stop punching and grabbing
func attack_stop():
	_cur_signal_state = T.Player.Action.NO_PUNCH
	pass